.. _contribution:

Contribution 
###############

.. important::
   First off, thanks for taking the time to think about contributing!

.. note::
   For donations, see `BeagleBoard.org - Donate <https://beagleboard.org/donate>`__.

The BeagleBoard.org Foundation maintains source for many open source projects.

Example projects suitable for first contributions:

* `BeagleBoard project documentation <https://openbeagle.org/docs/docs.beagleboard.io>`__
* `Debian image bug repository <https://openbeagle.org/beagleboard/Latest-Images>`__
* `Debian image builder <https://openbeagle.org/beagleboard/image-builder>`__

These guidelines are mostly suggestions, not hard-set rules. Use your best judgment, and feel free
to propose changes to this document in a merge request.

.. toctree::
   :maxdepth: 2

   code-of-conduct
   faq
   background
   how
   gsoc
   git-usage
   style
   rst-cheat-sheet
   linux-upstream
   /CONTRIB
